package name.hsrm.anum.bisection.gui;

import java.awt.EventQueue;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import name.hsrm.anum.bisection.implementation.Bisection;

public class Application {

	private JFrame frmBisektionsverfahren;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Application window = new Application();
					window.frmBisektionsverfahren.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Application() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmBisektionsverfahren = new JFrame();
		frmBisektionsverfahren.setTitle("Bisektionsverfahren");
		frmBisektionsverfahren.setResizable(false);
		frmBisektionsverfahren.setBounds(100, 100, 400, 385);
		frmBisektionsverfahren.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBisektionsverfahren.getContentPane().setLayout(null);
		
		JLabel lblStart = new JLabel("Start");
		lblStart.setBounds(12, 12, 70, 15);
		frmBisektionsverfahren.getContentPane().add(lblStart);
		
		JLabel lblEnde = new JLabel("Ende");
		lblEnde.setBounds(94, 12, 70, 15);
		frmBisektionsverfahren.getContentPane().add(lblEnde);
		
		JSpinner spinnerStart = new JSpinner();
		spinnerStart.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		spinnerStart.setBounds(12, 39, 70, 20);
		frmBisektionsverfahren.getContentPane().add(spinnerStart);
		
		JSpinner spinnerEnd = new JSpinner();
		spinnerEnd.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		spinnerEnd.setBounds(94, 39, 70, 20);
		frmBisektionsverfahren.getContentPane().add(spinnerEnd);
		
		TextArea results = new TextArea();
		results.setEditable(false);
		results.setBounds(10, 65, 374, 247);
		frmBisektionsverfahren.getContentPane().add(results);

		JButton btnLos = new JButton("Los!");
		btnLos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				results.setText("");
				results.append("Nullstellen werden gesucht für:");
				results.append(System.lineSeparator());
				results.append(Bisection.getFunctionString());
				results.append(System.lineSeparator());
				double startValue = (double) spinnerStart.getValue();
				double endValue = (double) spinnerEnd.getValue();
				double endResult = Bisection.bisect(startValue, endValue, results);
				results.append("Ergebnis:" +endResult);
			}
		});
		btnLos.setBounds(267, 318, 117, 25);
		frmBisektionsverfahren.getContentPane().add(btnLos);
	}
}
