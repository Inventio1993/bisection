package name.hsrm.anum.bisection.implementation;

import java.awt.TextArea;

public class Bisection {
	private static double MAX_DELTA = 0.00001;
	
	private static double f(double x) {
		//return 5 * Math.pow(x, 3) + 7 * Math.pow(x, 2) - 10 * x + 42;
		return 3*x*x*x - 4*x*x - 2*x + 2;
	}
	
	//Von einem engineering Standpunkt aus astrein... *hust*
	public static String getFunctionString() {
		return "3x^3-4x^2-2x+2";
	}

	public static double bisect(double startX, double endX, TextArea results) {
		addToResults(results, startX +" | " + endX);
		
		if ((f(startX) < 0) ^ (f(endX) < 0)) {
			if(Math.abs(startX - endX) > MAX_DELTA) {
				// Berechne Mitte
				double middle = (startX + endX) / 2;
				double newStartX;
				double newEndX;
				
				// Entscheide, ob mit linkem, oder rechtem
				// Intervall weitergerechnet wird
				if ((f(startX) * f(middle)) < 0) {
					newStartX = startX;
					newEndX = middle;
				} else {
					newStartX = middle;
					newEndX = endX;
				}
				
				return bisect(newStartX, newEndX, results);
			} else return startX;		
		} else {
			addToResults(results, "Es muss einen Vorzeichenwechsel von Intervallstart bis Intervallende geben!");
			return 0.0;
		}
	}
	
	private static void addToResults(TextArea results, String text) {
		results.append(text);
		results.append(System.lineSeparator());
	}
	
}